<?php

namespace App;

use App\Traits\MakesTransactions;
use Illuminate\Database\Eloquent\Model;

class CreditCard extends Model
{
	use MakesTransactions;

	protected $fillable = ['name_on_card', 'card_number', 'exp_date', 'cvv'];
	protected $hidden = ['card_number', 'exp_date', 'cvv'];
	protected $appends = ['last_digits'];

	public function getLastDigitsAttribute()
	{
		return substr($this->card_number, -4);
	}
}
