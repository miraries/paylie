<?php

namespace App\Traits;

trait MakesTransactions
{
	public function calculate()
	{
		$this->balance = $this->receivedTransactions()->sum('amount') - $this->sentTransactions()->sum('amount');

		return $this;
	}

	public function sentTransactions()
	{
		return $this->morphMany('App\Transaction', 'sender');
	}

	public function receivedTransactions()
	{
		return $this->morphMany('App\Transaction', 'receiver');
	}

	public function transactions()
	{
		return $this->sentTransactions()->union($this->receivedTransactions()->toBase())->orderByDesc('created_at');
	}
}
