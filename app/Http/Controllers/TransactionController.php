<?php

namespace App\Http\Controllers;

use App\CreditCard;
use App\Transaction;
use App\User;
use App\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    public function index()
    {
    	$user = Auth::user();
        return $user->vendor()->exists() ? $user->vendor->transactions : $user->transactions;
    }

	public function show(Transaction $transaction)
	{
		return $transaction;
	}

	/**
	 * @param Request $request
	 * @param User $user
	 * @return Transaction
	 * @throws \Illuminate\Validation\ValidationException
	 */
	public function sendUser(Request $request, User $user)
    {
	    $this->validate($request, [
		    'amount' => 'required|numeric'
	    ]);

	    $t = new Transaction(['amount' => $request->amount]);
	    $t->sender()->associate(Auth::user());
	    $t->receiver()->associate($user);
	    $t->save();

	    $user->calculate()->save();
	    Auth::user()->calculate()->save();

	    return $t;
    }

	/**
	 * @param Request $request
	 * @param Vendor $vendor
	 * @return Transaction
	 * @throws \Illuminate\Validation\ValidationException
	 */
	public function sendVendor(Request $request, Vendor $vendor)
	{
		$this->validate($request, [
			'amount' => 'required|numeric'
		]);

		$t = new Transaction(['amount' => $request->amount]);
		$t->sender()->associate(Auth::user());
		$t->receiver()->associate($vendor);
		$t->save();

		$vendor->calculate()->save();
		Auth::user()->calculate()->save();

		return $t;
	}

	/**
	 * @param Request $request
	 * @param CreditCard $card
	 * @return Transaction
	 * @throws \Illuminate\Validation\ValidationException
	 */
	public function transferIn(Request $request, CreditCard $card)
	{
		$this->validate($request, [
			'amount' => 'required|numeric'
		]);

		$t = new Transaction(['amount' => $request->amount]);
		$t->sender()->associate($card);
		$t->receiver()->associate(Auth::user());
		$t->save();

		Auth::user()->calculate()->save();

		return $t;
	}

	/**
	 * @param Request $request
	 * @param CreditCard $card
	 * @return Transaction
	 * @throws \Illuminate\Validation\ValidationException
	 */
	public function transferOut(Request $request, CreditCard $card)
	{
		$this->validate($request, [
			'amount' => 'required|numeric'
		]);

		$t = new Transaction(['amount' => $request->amount]);
		$t->sender()->associate(Auth::user());
		$t->receiver()->associate($card);
		$t->save();

		Auth::user()->calculate()->save();

		return $t;
	}
}
