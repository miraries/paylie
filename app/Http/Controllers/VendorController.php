<?php

namespace App\Http\Controllers;

use App\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VendorController extends Controller
{
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return Vendor|\Illuminate\Database\Eloquent\Model
	 * @throws \Illuminate\Validation\ValidationException
	 */
    public function store(Request $request)
    {
    	if(Auth::user()->vendor()->exists())
    		abort(403, 'Cannot register more than one vendor');

	    $this->validate($request, [
		    'name' => 'required|string|max:255',
		    'email' => 'required|email',
		    'vat_no' => 'required|string|max:50',
		    'bank_account' => 'required|string|max:50',
	    ]);

	    $vendor = Vendor::create($request->all());
	    Auth::user()->vendor()->associate($vendor)->save();

	    return $vendor;
    }

	/**
	 * Display the specified resource.
	 *
	 * @param \App\Vendor $vendor
	 * @return Vendor
	 */
    public function show(Vendor $vendor)
    {
        return $vendor;
    }

    public function associate(Vendor $vendor)
    {
	    if(Auth::user()->vendor()->exists())
		    abort(403, 'You are already associated with a vendor');

	    Auth::user()->vendor()->associate($vendor)->save();

	    return $vendor;
    }
}
