<?php

namespace App\Http\Controllers;

use App\CreditCard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CreditCardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Auth::user()->creditCards()->orderByDesc('created_at')->get();
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @return \Illuminate\Database\Eloquent\Model
	 * @throws \Illuminate\Validation\ValidationException
	 */
    public function store(Request $request)
    {
	    $this->validate($request, [
		    'name_on_card' => 'required|string|max:255',
		    'card_number' => 'required|string|size:16',
		    'exp_date' => 'required|regex:/^\d{2}\/\d{2}$/i',
		    'cvv' => 'required|max:999',
	    ]);

	    return Auth::user()->creditCards()->create($request->all());
    }

	/**
	 * Display the specified resource.
	 *
	 * @param CreditCard $card
	 * @return CreditCard
	 */
    public function show(CreditCard $card)
    {
        if($card->user_id !== Auth::user()->id) abort(403);

        return $card;
    }
}
