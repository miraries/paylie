<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
	protected $fillable = ['amount'];
	protected $with = ['sender', 'receiver'];

	public function sender()
	{
		return $this->morphTo('sender');
	}

	public function receiver()
	{
		return $this->morphTo('receiver');
	}
}
