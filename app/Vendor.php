<?php

namespace App;

use App\Traits\MakesTransactions;
use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
	use MakesTransactions;

	protected $fillable = ['name', 'email', 'vat_no', 'bank_account'];
}
