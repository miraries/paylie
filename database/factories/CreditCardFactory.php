<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\CreditCard;
use Faker\Generator as Faker;

$factory->define(CreditCard::class, function (Faker $faker) {
    return [
	    'name_on_card' => $faker->name,
	    'card_number' => $faker->creditCardNumber,
	    'exp_date' => $faker->creditCardExpirationDateString,
	    'cvv' => $faker->randomNumber(3, true)
    ];
});
