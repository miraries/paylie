<?php

use App\CreditCard;
use App\User;
use App\Vendor;
use App\Transaction;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
	private $transactables = [
		Vendor::class,
		User::class,
		CreditCard::class,
	];
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{
		/** @var Vendor $vendor */

		$this->command->info('Seeding sample data...');

		factory(User::class, 10)->create();
		$vendors = factory(Vendor::class, 5)->create();

		foreach ($vendors as $vendor) {
			factory(User::class)->create(['vendor_id' => $vendor->getKey()]);
		}

		foreach (User::get() as $user) {
			factory(\App\CreditCard::class, rand(1, 3))->create(['user_id' => $user->getKey()]);
		}

		foreach (User::get() as $user) {
			for ($i = 0; $i <= 15; $i++) {
				$this->customTransactionFactory($user)->save();
			}
		}

		foreach ([Vendor::class, User::class] as $transactable){
			foreach ($transactable::get() as $entity){
				$entity->calculate()->save();
			}
		}
	}

	private function customTransactionFactory($user)
	{
		/** @var User $user */

		$faker = Faker::create();


		$transactableType = $faker->randomElement($this->transactables);

		if ($transactableType == CreditCard::class) {
			$transactable = $user->creditCards()->inRandomOrder()->first();
		} else {
			if ($transactableType == User::class) {
				$transactable = User::whereKeyNot($user->getKey())->inRandomOrder()->first();
			} else {
				$transactable = $transactableType::inRandomOrder()->first();
			}
		}

		if ($faker->boolean) {
			return new Transaction([
				'amount' => $faker->randomFloat(2, 0, 300),
				'sender_type' => array_search($transactableType,
					\Illuminate\Database\Eloquent\Relations\Relation::$morphMap),
				'sender_id' => $transactable->getKey(),
				'receiver_type' => 'user',
				'receiver_id' => $user->getKey()
			]);
		} else {
			return new Transaction([
				'amount' => $faker->randomFloat(2, 0, 300),
				'sender_type' => 'user',
				'sender_id' => $user->getKey(),
				'receiver_type' => array_search($transactableType,
					\Illuminate\Database\Eloquent\Relations\Relation::$morphMap),
				'receiver_id' => $transactable->getKey()
			]);
		}
	}
}
