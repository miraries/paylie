<h1 align="center">Welcome to paylie 👋</h1>

<img alt="Project banner" src="docs/banner.jpg">

<p>
  <img alt="Version" src="https://img.shields.io/badge/version-0.3.0-blue.svg?cacheSeconds=2592000" />
  <a href="#" target="_blank">
    <img alt="License: GNU GPLv3" src="https://img.shields.io/badge/License-GNU GPLv3-yellow.svg" />
  </a>
</p>

> A demo of a proposed system as part of a class project whose goal is to enable seamless payments and money transfer between regular users and vendors, with AliPay and WeChat Pay being the primary influences.

### ✨ Live Demo (TBD)

### [Screenshots](docs/screenshots/Screenshots.md)

## Documentation

### [Full project documentation (not in English)](docs/Paylie.pdf)

## API Reference (Postman)

### [Available here](https://documenter.getpostman.com/view/1392368/S1Zw9BS5?version=latest)

## Notable tech used

- Laravel
- Vue
- Vuex
- Axios
- Bootstrap
- Moment
- Sweetalert2
- Vue-i18n
- Fontawesome-vue
- Vue-QR
- Vue-QRCode-Reader
- Vue-Credit-Card

Authentication is done using JWT.  
An interesting part of the project is making use of Laravel's Polymorphic tables - the `transactions` table has the `sender` and `receiver` columns, each of which can contain three different models depending on the type of transaction.  
So a transaction can exist between any of the following models:
- `User`
- `Vendor`
- `CreditCard`

## Install


```sh
composer install
```

Cope `.env.example` to `.env`. Edit .env and set your database connection details.

```sh
php artisan key:generate && php artisan jwt:secret
php artisan migrate
npm install
```

## Usage - Development

```sh
php artisan serve
```

```sh
npm run serve
```

## Usage - Production

```sh
npm run build
```

## Graphics generated as part of project

### Mind map

<img alt="Project banner" src="docs/mindmap.jpg">

### Schema

<img alt="Project banner" src="docs/schema.jpg">

## Authors

👤 **Ivan Kotlaja**
* Website: [ivnk.dev](https://ivnk.dev)

👤 **Petar Djurickovic**
