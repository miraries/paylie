function page (path) {
  return () => import(/* webpackChunkName: '' */ `~/pages/${path}`).then(m => m.default || m)
}

export default [
  { path: '/', name: 'welcome', component: page('welcome.vue') },

  { path: '/login', name: 'login', component: page('auth/login.vue') },
  { path: '/register', name: 'register', component: page('auth/register.vue') },
  { path: '/password/reset', name: 'password.request', component: page('auth/password/email.vue') },
  { path: '/password/reset/:token', name: 'password.reset', component: page('auth/password/reset.vue') },
  { path: '/email/verify/:id', name: 'verification.verify', component: page('auth/verification/verify.vue') },
  { path: '/email/resend', name: 'verification.resend', component: page('auth/verification/resend.vue') },

  { path: '/home', name: 'home', component: page('home.vue') },
  { path: '/settings',
    component: page('settings/index.vue'),
    children: [
      { path: '', redirect: { name: 'settings.profile' } },
      { path: 'profile', name: 'settings.profile', component: page('settings/profile.vue') },
      { path: 'password', name: 'settings.password', component: page('settings/password.vue') }
    ] },

  { path: '/cards', name: 'cards', component: page('cards.vue') },
  { path: '/add_card', name: 'add.card', component: page('add_card.vue') },
  { path: '/send_money', name: 'send.money', component: page('send_money.vue') },
  { path: '/receive_money', name: 'receive.money', component: page('receive_money.vue') },
  { path: '/transactions', name: 'transactions', component: page('transactions.vue') },
  { path: '/card_transfer/:id', name: 'card.transfer', component: page('card_transfer.vue') },
  { path: '/associate/:id', name: 'vendor.associate', component: page('associate.vue') },
  { path: '/add_vendor', name: 'add.vendor', component: page('add_vendor.vue') },

  { path: '*', component: page('errors/404.vue') }
]
