<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => 'auth:api'], function () {
	Route::get('cards', 'CreditCardController@index');
	Route::get('cards/{card}', 'CreditCardController@show');
	Route::post('cards', 'CreditCardController@store');

	Route::get('vendors/{vendor}', 'VendorController@show');
	Route::post('vendors', 'VendorController@store');
	Route::post('vendors/{vendor}/associate', 'VendorController@associate');

	Route::get('users/{user}', 'Settings\ProfileController@show');

	Route::get('transactions/', 'TransactionController@index');
	Route::get('transactions/{transaction}', 'TransactionController@show');

	Route::post('transactions/send/vendor/{vendor}', 'TransactionController@sendVendor');
	Route::post('transactions/send/user/{user}', 'TransactionController@sendUser');
	Route::post('transactions/transfer/in/{card}', 'TransactionController@transferIn');
	Route::post('transactions/transfer/out/{card}', 'TransactionController@transferOut');
});


Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', 'Auth\LoginController@logout');

    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::patch('settings/profile', 'Settings\ProfileController@update');
    Route::patch('settings/password', 'Settings\PasswordController@update');
});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', 'Auth\LoginController@login');
    Route::post('register', 'Auth\RegisterController@register');

    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::post('email/verify/{user}', 'Auth\VerificationController@verify')->name('verification.verify');
    Route::post('email/resend', 'Auth\VerificationController@resend');

    Route::post('oauth/{driver}', 'Auth\OAuthController@redirectToProvider');
    Route::get('oauth/{driver}/callback', 'Auth\OAuthController@handleProviderCallback')->name('oauth.callback');
});

